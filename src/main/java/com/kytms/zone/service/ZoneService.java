package com.kytms.zone.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.model.TreeModel;
import com.kytms.core.service.BaseService;

import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-23
 */
public interface ZoneService<Zone> extends BaseService<Zone> {
    List<TreeModel> getZoneTree(String id);
    List<TreeModel> getZoneTree(String roleId,String id);

    List<Zone> getZoneGrid(CommModel commModel);

    List<TreeModel> getCity(CommModel commModel);

    List<Zone> selectAllZone();

    List<TreeModel> getAll(CommModel commModel);
}
